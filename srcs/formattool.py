def formattool(data, time, timelocks, binsize, maxLead, maxLag, method):

    '''
    Converts simple data format to dataraster format
    Organizes data from simple formats (e.g. voltage value vectors) to the
    dataraster format for processing in the information analysis. For each
    point in timelocks, data will be binned around the time lock point. One
    bin will start for time values equal to or greater than the timelock
    point and extending binsize time out in the future. Other bins will be
    added before and after the timelock point using identical boundary
    patterns. 

    Syntax: [formdata,timeboundaries] = formattool(data, time, timelocks, binsize, maxLead, maxLag, method)

    Input:
        data (1 by nObs double array) - basic data to be converted. The data
            can be continuous data (e.g. voltage values), or it can be discrete
            (e.g. integer stimulus or behavior states, integer spike states (1= spike). 
            nObs is the total number of observations.
        
        time (1 by nObs double array) - time values corresponding to the basic
            data values in the data array. nObs is the total number of
            observations. All variables using time must have the same units.
        
        timelocks (1 by nTrials double array) - time lock points. These times will be used
            allign sections of data in the dataraster. nTrials is the total number of
            trials. All variables using time must have the same units.
   
        binsize (double) - the size of the time bins. All variables using time
            must have the same units.
   
        maxLead (double) - the maximum amount of time analyzed prior to the
            time lock points. All variables using time must have the same units.
   
        maxLag (double) - the maximum amount of time analyzed after the time
            lock points. All variables using time must have the same units.
   
        method (string) - the method to be used to bin the data. The method is 
            only relevant if the bin size is changed. Possible choices are:
           
           'count': total count of the data values in a bin. This is most
               useful for spike counts where spikes are noted as 1 in the
               data array.
           
           'ave': average data values. This can be useful for continuous
               valued data (e.g. voltage data).

    Outputs:
        formdata (1 by nbins by nTrials double array) - the original data in
            the dataraster format. The data can be easily added to a larger 
            array with other data to form a large dataraster, which can then be 
            processed by data2states.

        timeboundaries (nbins by 2 double array) - the boundaries for the
            corresponding time bin in formdata. The first column represents the
            earliest time in the bin and the second column represents the
            latest time in the bin. For instance, if timeboundaries(5,:) =
            [1,2], formdata(1,5,8) includes data at time points greater than or 
            equal to 1 time units, but less than 2 time units after timelocks(8).

    ------

    Version 1.0

    Version Information:

    1.O: 13/08/2020 - The original version of the program was created before
    and modified it. (QiangLi)
  
    Copyright (c) 2020, University of Valencia
    All rights reserved.

    Authors: QiangLi (qiang.li@uv.es)

    -------
    Liences
    -------
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    1. Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.
    
    2. Redistributions in binary form must reproduce the above copyright notice,
        this list of conditions and the following disclaimer in the documentation
        and/or other materials provided with the distribution.

    3. Neither the name of University of Valencia nor the names of its contributors
        may be used to endorse or promote products derived from this software
        without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
    
    '''
    
    import numpy as np
    import sys

    # Error check the time and data vectors
    try:
        len(data) != len(time)
    except EnvironmentError:
        print('data and timee must be equal length')
    
    # Figure out the bin boundaries
    TimeBounds = (-np.floor(maxLead/binsize):np.floor(maxLag/binsize)) * binsize

    # Prellocate the formatted data
    nbins = len(TimeBounds) - 1
    nTrils = len(timelocks)
    formdata = np.NaN([1, nbins, nTrils])
    timeboundaries = np.NaN([nbins,2])

    # Make the timeboundaries list
    for iBin in range(1, nbins):
        timeboundaries[iBin,:] = (TimeBounds[iBin], TimeBounds[iBin+1])

    # Process the data

    if method == 'count':
        for i in range(1, nTrils):
            for iBin in range(1, nbins):
                formdata[1,iBin,iTrial] = np.sum(data[(time >= (timelocks[iTrial] + ...
                    TimeBounds[iBin])) and (time < (timelocks[iTrial] + TimeBounds[iBin + 1]))])

    elif method =='ave':
        for i in range(1, nTrils):
            for iBin in range(1, nbins):
                formdata[1,iBin,iTrial] = np.mean(data[(time >= (timelocks[iTrial] + ...
                    TimeBounds[iBin])) and (time < (timelocks[iTrial] + TimeBounds[iBin + 1]))])

