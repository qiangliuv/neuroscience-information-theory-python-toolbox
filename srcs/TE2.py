def TE2(CountsMat):

    '''
    TE calculates the transfer entropy from the X2
    variable to the Y variable assuming the X1 variable is the history of the
    signal or process that preceeded Y. It is based on the transfer entropy
    paper by Schrieber:

    T. Schreiber, Measuring Information Transfer, PRL 85 (2000).

    
    Input:

    CountsMat is a tensor that contains the counts of the various states of the
        variables. The first index corresponds to the state of the Y variable.
        The second through N+1 indexes correspond to the states of the X1 to XN
        variables. New to version 2 is the ability to use counts matrices with an
        N+2 index that corresponds to a different data set.

    Output:
        TE
     ------

    Version 1.0

    Version Information:

    1.O: 13/08/2020 - The original version of the program was created before
    and modified it. (QiangLi)
  
    Copyright (c) 2020, University of Valencia
    All rights reserved.

    Authors: QiangLi (qiang.li@uv.es)

    -------
    Liences
    -------
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    1. Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.
    
    2. Redistributions in binary form must reproduce the above copyright notice,
        this list of conditions and the following disclaimer in the documentation
        and/or other materials provided with the distribution.

    3. Neither the name of University of Valencia nor the names of its contributors
        may be used to endorse or promote products derived from this software
        without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
    
    '''
    import numpy as np

    ny,nx1,nx2,nd=CountsMat.shape
    TotalCounts=np.sum(np.sum(np.sum(CountsMat, 1),2),3)
    P=np.divide(CountsMat, TotalCounts[np.ones([1,ny]),ones([1,nx1]),ones([1,nx2]),:])

    Px1x2=np.sum(P,1)

    PyGivx1x2= np.divide(P, Px1x2[ones([1,ny]),:,:,:])

    Px1=np.sum(np.sum(P,1),3)
    Pyx1=np.sum(P,3)

    PyGivx1=np.divide(Pyx1[:,:,ones([1,nx2]),:], Px1[ones([1,ny]),:,ones([1,nx2]),:])

    TE=np.dot(P, np.log2(np.divide(PyGivx1x2,PyGivx1)))

    TE[not np.isfinite(TE)]=0
    TE=np.squeeze(np.sum(np.sum(np.sum(TE,1),2),3))
