def MutualInfo(CountsMat):

  '''
  MutualInfo calculates the mutual information between Y and {X1,...XN}.
  [MI] = MutualInfo(CountsMat) is the mutual information between the Y
  variable and the X variables considered as one vector valued variable
  {X1, ... XN}.

  -----
  Input:
  
  CountsMat: An array that contains the counts (or joint probability  values) 
  of the various states of the variables. The first index corresponds to the 
  state of the Y variable. The second through N+1 indexes correspond to the 
  states of the X1 to XN variables. 


  Outputs:

  MI: The mutual information between Y and {X1, ... XN} in bits.
  -----

  Version 1.0

  Version Information:

    1.O: 29/06/2020 - The original version of the program was created before
    and modified it. (QiangLi)

  Copyright (c) 2020, University of Valencia
  All rights reserved.

  Authors: Qiang Li (qiang.li@uv.es)

  -------
  Liences
  -------
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 
  2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  3. Neither the name of University of Valencia nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
  '''
  import numpy as np

  #Obtain the number of states for each variable
  nS = np.size(CountsMat)

  #Obtain the number of X variables
  N = len(nS)-1

  #Convert the CountsMat to a joint probability distribution
  Pxy = CountsMat/np.sum([CountsMat[:]])

  #Find the joint probabilities for the Y and {X1,...XN} variables
  Px = np.matrixlib.repmat(np.sum(Pxy,axis=0),[nS[1],np.ones([1,N])])
  Py = np.matrixlib.repmat(np.sum(Pxy[:,:],axis=1),[1,nS(2:)])

  #Calculate the mutual information
  temp = np.dot(Pxy, np.log2(np.divide(Pxy, np.dot(Px, Py))))

  #Matlab incorrectly gives states with Pxy = 0 a non-finite value.
  temp[not np.isfinite(temp)] =0

  #Sum over the terms to get the mutual information
  MI = np.sum(temp[:])

  return MI