def EntropyY(CountsMat):

  '''
  EntropyY calculates the entropy in bits of all the Y variable in
  CountsMat.

  [ENT] = EntropyY(CountsMat) is the entropy of the the Y variable in
  CountsMat.

  -------
  Inputs:
  
  CountsMat: An array that contains the counts (or joint probability 
  values) of the various states of the variables. The first index 
  corresponds to the state of the Y variable. The second through N+1 
  indexes correspond to the states of the X1 to XN variables. 
  
  Outputs:

  ENT: Entropy of the Y variable.
  ------

  Version 1.0

  Version Information:

    1.O: 29/06/2020 - The original version of the program was created before
    and modified it. (QiangLi)
  
  Copyright (c) 2020, University of Valencia
  All rights reserved.

  Authors: QiangLi (qiang.li@uv.es)

  -------
  Liences
  -------
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 
  2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  3. Neither the name of University of Valencia nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
  
  '''
  import numpy as np

  #Convert the CountsMat to a joint probability distribution
  Pxy = CountsMat/np.sum([CountsMat[:]])

  #Find the probability distribution of the Y variable
  Py = np.sum(Pxy[:,:], axis=1)

  #Calculate the entropy
  Temp = np.dot(-Py, np.log2(Py))
  Temp[not np.isfinite(Temp)] = 0
  ENT = np.sum([Temp(:)])

  return ENT