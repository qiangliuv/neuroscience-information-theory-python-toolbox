def  instinfo(StatesRaster, Method, VariableIDs, *args):
    '''
    INSTINFO - instantaneous information analysis
    Calculates various information theoretic quantities for trial based or
    single trial data. Single trial data are analyzed through time, while
    trial based data are analyzed across trials at each time bin.

    Syntax: [InfoVal,p,s,MCInfoVals] = instinfo(StatesRaster, Method, VariableIDs, varargin)

    Input:
        StatesRaster (cell array or double array) - trial state data. If a
        time bins by number of trials. Each element should be an integer
        state number (state number = 1, 2, 3, ...). If a cell array is used,
        it should have only one dimension and each element should be a double
        array with the dimensions listed above. Each element of the cell
        array is referred to as a 'data category'.
        Method (string) - sets the type of information measure to apply. (See
        option sets below)
        VariableIDs (cell array) - sets the variables used in the information
        measure. The array is generally N by 3 where N is the number of 
        variables. The first column contains an integer that sets the data 
        category. If there is only one data category, this column can be 
        removed. The second column contains the variable number in the data 
        category. This number corresponds to the first subscript in the 
        StatesRaster array. The third column contains the time bin value. 
        This corresponds to the second subscript in the StatesRaster array. 
        (See option sets below.)
        *Side Note: For single trial data, the precise values of the time
        bins in the third column are not relevant, only the relative values
        will affect the analysis. The relative values will set delays
        between the states used. For instance, for Pairwise Mutual
        Information, {1,5,2;1,1,3} is equivalent to {1,5,5;1,1,6} because 
        3 - 2 = 6 - 5, though different from {1,5,2;1,1,4} because 
        3 - 2 ~= 4 - 2. Positive integers must be used. Frequently, it is
        best to set the earliest variable as time 1 and proceed from there.

    Input Option Sets
        Entropy - measures the entropy of a given source. The source to be used
        is listed in the VariableIDs. The Monte Carlo options do not affect 
        entropy calculations. InfoVal output will be a single number in units
        of bits.
        Example 1
        Method = 'Ent'
        VariableIDs = {1,7,3}
        Explanation: These inputs will produce an entropy calculation for
        data category 1 variable 7 at time bin 3.
        Example 2
        Method = 'Ent'
        VariableIDs = {3,2,5}
        Explanation: These inputs will produce an entropy calculation for
        data category 3 variable 2 at time bin 5.
        Joint Entropy - measures the entropy of several sources jointly. The
        sources to be used are listed in the VariableIDs. The Monte Carlo
        options do not affect entropy calculation. InfoVal output will be a
        single number in units of bits.
        Example 1
        Method = 'JointEnt'
        VariableIDs = {1,7,3;...
                    2,5,6}
        Explanation: These inputs will produce a joint entropy calculation 
        for data category 1 variable 7 at time bin 3 and data category 2
        variable 5 at time bin 6.
        Example 2
        Method = 'JointEnt'
        VariableIDs = {1,7,3;...
                    2,5,6;...
                    2,9,10}
        Explanation: These inputs will produce a joint entropy calculation 
        for data category 1 variable 7 at time bin 3, data category 2
        variable 5 at time bin 6, and data category 2 variable 9 at time
        bin 10.
        Joint Conditional Entropy - measures the conditional entropy between any
        two groups of sources, each taken jointly. The sources are listed in 
        VariableIDs. The grouping is indicated using the method name 
        ('CondEnti' where the first i variables listed in VariableIDs will be
        considered as one joint set and conditioned on the remaining variable
        considered as another joint set, with i being less than or equal to
        9). The Monte Carlo options do not affect entropy calculation. 
        InfoVal output will be a single number in units of bits.
        Example 1
        Method = 'CondEnt1'
        VariableIDs = {1,7,3;...
                    2,5,6}
        Explanation: These inputs will produce a conditional entropy 
        calculation with data category 1 variable 7 at time bin 3 
        conditioned on data category 2 variable 5 at time bin 6.
        Example 2
        Method = 'CondEnt2'
        VariableIDs = {1,7,3;...
                    2,5,6;...
                    2,9,10}
        Explanation: These inputs will produce a conditonal entropy 
        calculation with data category 1 variable 7 at time bin 3 jointly
        with data category 2 variable 5 at time bin 6 on data category 2
        variable 9 at time bin 10. 
        Pairwise Mutual Information - measures the pairwise mutual information 
        between any two sources. The sources are listed in VariableIDs. If 
        the Monte Carlo option is selected, the first variable will be 
        randomized, though this is equivalent to randomizing the second 
        variable because the joint distribution is bivariate. InfoVal output 
        will be a single number in units of bits.
        Example 1
        Method = 'PairMI'
        VariableIDs = {1,6,5;...
                    1,10,5}
        Explanation: These inputs will produce a pairwise mutual
        information measurement between data category 1 variables 6 and 10 
        contemporaneously at time bin 5.
        Example 2
        Method = 'PairMI'
        VariableIDs = {8,5,2;...
                    9,1,3}
        Explanation: These inputs will produce a pairwise mutual
        information measurement between data category 8 variable 5 at time 
        bin 2 and data category 9 variable 1 at time bin 3. 
        Joint Mutual Information - measures the mutual information between any
        two groups of sources, each taken jointly. The sources are listed in 
        VariableIDs. The grouping is indicated using the method name 
        ('JointMIi' where the first i variables listed in VariableIDs will be
        considered as one joint set and the remaining variables will be 
        considered as the other joint set, with i being less than or equal to
        9). If the Monte Carlo option is selected, the first joint set of 
        variables will be randomized together, though this is equivalent to 
        randomizing the second joint set of variables because the 
        distribution is bivariate. InfoVal output will be a single number in 
        units of bits. 
        Example 1
        Method = 'JointMI2'
        VariableIDs = {1,6,5;...
                    4,10,5;...
                    9,2,3}
        Explanation: These inputs will produce a mutual information
        measurement between the joint set of data category 1 variable 6 at
        time bin 5 with data category 4 variable 10 at time bin 5, and data
        category 9 variable 2 at time bin 3.
        Example 2
        Method = 'JointMI1'
        VariableIDs = {6,5,2;...
                    7,1,3;...
                    8,4,10}
        Explanation: These inputs will produce a mutual information
        measurement between data category 6 variable 5 at time bin 2, and
        the joint set of data category 7 variable 1 at time bin 3 with data
        category 8 variable 4 at time bin 10. 
        Transfer Entropy - measures the transfer entropy between any two
        sources. The first variable listed in the VariableIDs will be the 
        receiving variable in the future, the second variable listed will be 
        the receiving variable in the past, and the third variable listed 
        will be the transmitting variable in the past. Typically, the time 
        bin for both past variables are prior to the future state (often one 
        time bin earlier). An error will be issued if the times for the past 
        states are later than or equal to the time for the future state. If 
        the Monte Carlo option is selected, only the transmitter variable 
        will be randomized to preserve the autoprediction in the receiver. 
        InfoVal output will be a single number in units of bits.
        Method = 'TE'
        VariableIDs = {1,3,7;...
                        1,3,6;...
                        2,8,6}
        Explanation: These inputs will calculate TE with the transmitter
        variable being data category 2 variable 8 at time bin 6, the 
        receiver past state will be data category 1 variable 3 at time bin 
        6, and the receiver future state will be data category 1 variable 3
        at time bin 7.
        Example 2
        Method = 'TE'
        VariableIDs = {3,5,7;...
                        3,5,6;...
                        1,2,4}
        Explanation: These inputs will calculate TE with the transmitter
        variable being data category 1 variable 2 at time bin 4, the 
        receiver past state will be data category 3 variable 5 at time bin 
        6, and the receiver future state will be data category 3 variable 5
        at time bin 7.
    2-Variable PID - measures the 2 X variable, 1 Y variable partial
        information terms. The first variable in VariableIDs will be the Y 
        variable. The second and third variables in VariableIDs will be the 
        first and second X variables, respectively. If the Monte Carlo option
        is selected, both of the X variables will be randomized. InfoVal 
        output will contain four values (redundancy, unique X1, unique X2, 
        and synergy). The p output will contain 4 p-values (corresponding to 
        the terms in InfoVal). 
        Example 1
        Method = '2PID'
        VariableIDs = {1,12,3;...
                        4,8,3;...
                        4,2,3}
        Explanation: These inputs will calculate the 2-variable PID with
        data category 1 variable 12 at time bin 3 as the Y variable, data 
        category 4 variable 8 at time bin 3 as the X1 variable, and data 
        category 4 variable 2 at time bin 3 as the X2 variable.
        Example 2
        Method = '2PID'
        VariableIDs = {2,1,5;...
                        8,3,7;...
                        9,4,6}
        Explanation: These inputs will calculate the 2-variable PID with
        data category 2 variable 1 at time bin 5 as the Y variable, data 
        category 8 variable 3 at time bin 7 as the X1 variable, and data 
        category 9 variable 4 at time bin 6 as the X2 variable.
    3-Variable PID - measures the 3 X variable, 1 Y variable partial
        information terms. The first variable in VariableIDs will be the Y 
        variable. The second,  third, and fourth variables in VariableIDs 
        will be the first, second, and third X variables, respectively. If 
        the Monte Carlo option is  selected, all three X variables will be 
        randomized. InfoVal output will contain 18 values (see PIDTermLabels 
        for term identity). The p output will contain 18 p-values 
        (corresponding to the terms in InfoVal).
        Example 1
        Method = '3PID'
        VariableIDs = {1,12,5;...
                        3,8,5;...
                        3,2,5;...
                        4,7,5}
        Explanation: These inputs will calculate the 3-variable PID with
        data category 1 variable 12 at time bin 5 as the Y variable, data 
        category 3 variable 8 at time bin 5 as the X1 variable, data 
        category 3 variable 2 at time bin 5 as the X2 variable, and data 
        category 4 variable 7 at time bin 5 as the X3 variable.
        Example 2
        Method = '3PID'
        VariableIDs = {9,1,5;...
                        10,2,6;...
                        11,3,7;...
                        12,4,8}
        Explanation: These inputs will calculate the 2-variable PID with
        data category 9 variable variable 1 at time bin 5 as the Y 
        variable, data category 10 variable 2 at time bin 6 as the X1 
        variable, data category 11 variable 3 at time bin 7 as the X2 
        variable, and data category 12 variable 4 at time bin 8 as the X3 
        variable.
    3-Variable TE - measures the multivariate transfer entropy from 2
        sources to a third source. The first variable listed in the
        VariableIDs will be the receiving variable in the future. The second
        variable listed in the VariableIDs will be the receiving variable in
        the past. The third variable listed in the VariablesIDs will the
        first transmitting variable in the past. The fourth variable listed
        in the VariablesIDs will be the second transmitting variable in the
        past. If any of the past variables have times later than or equal to
        the future state, an error will be issued. If the Monte Carlo option 
        is selected, only the transmitter variables will be randomized to
        preserve the autoprediction in the receiver. InfoVal output will have
        4 values (redundancy, unique transmitter 1, unique transmitter 2,
        synergy), each in units of bits. The p output will have 4 p-values
        corresponding to each InfoVal.
        Example 1
        Method = '3TE'
        VariableIDs = {2,1,10;...
                        2,1,9;...
                        3,5,9;...
                        4,6,9}
        Explanation: These inputs will calculate the 3-variable TE with
        data category 2 variable 1 at time bin 10 as the future state of
        the receiver, data category 2 variable 1 at time bin 9 as the past 
        state of the receiver, data category 3 variable 5 at time bin 9 as 
        the state of transmitter 1, and data category 4 variable 6 at time 
        bin 9 as the state of transmitter 2.
        Example 2
        Method = '3TE'
        VariableIDs = {3,1,10;...
                        3,1,9;...
                        4,5,7;...
                        4,2,7}
        Explanation: These inputs will calculate the 3-variable TE with
        data category 3 variable 1 at time bin 10 as the future state of
        the receiver, data category 3 variable 1 at time bin 9 as the past 
        state of the receiver, data category 4 variable 5 at time bin 7 as 
        the state of transmitter 1, and data category 4 variable 2 at time 
        bin 7 as the state of transmitter 2.
    Information Gain - measures the information gained by one variable
        (call it the receiver) about another variable (call it the signal) 
        beyond the information present in the receiver about the signal in 
        the past. The first variable listed in VariableIDs will be the 
        receiver in the future. The second variable listed in VariableIDs
        will be the receiver in the past. The third variable listed in
        VariableIDs will be the signal. An error will be produced if the past
        state is after or at the same time as the future state. If the Monte 
        Carlo option is used, the signal states are randomized. InfoVal will 
        have one value measured in bits.
        Example 1
        Method = 'InfoGain'
        VariableIDs = {2,4,6;...
                        2,4,5;...
                        3,1,5}
        Explanation: These inputs will calculate the information gained by
        data category 2 variable 4 at time bin 6 about data category 3 
        variable 1 at time bin 5 beyond the information present in data 
        category 2 variable 4 at time bin 5 about data category 3 variable 
        1 at time bin 5.
        Example 2
        Method = 'InfoGain'
        VariableIDs = {7,4,6;...
                        7,4,5;...
                        2,1,3}
        Explanation: These inputs will calculate the information gained by
        data category 7 variable 4 at time bin 6 about data category 2 
        variable 1 at time bin 3 beyond the information present in data 
        category 7 variable 4 at time bin 5 about data category 2 variable 
        1 at time bin 3.
    Information Transmission - measures the information transmitted from
        one source (call it the transmitter) to another source (call it the
        receiver) about a third source (call it the signal). The first
        variable listed in VariableIDs is the receiver in the future, the
        second is the receiver in the past, the third is the signal, and the
        fourth is the transmitter in the past. An error will be issued if
        the past states are after the future states. If the Monte Carlo 
        option is used, the signal states are randomized. InfoVal will have 
        one value measured in bits.
        Example 1
        Method = 'InfoTrans'
        VariableIDs = {2,4,6;...
                        2,4,5;...
                        3,1,5;...
                        8,12,5}
        Explanation: These inputs will calculate the information
        transmitted from data category 8 variable 12 at time bin 5 to data 
        category 2 variable 4 at time bin 6 about data category 3 
        variable 1 at time bin 5.
        Example 2
        Method = 'InfoTrans'
        VariableIDs = {2,4,6;...
                        2,4,5;...
                        9,1,3;...
                        8,12,3}
        Explanation: These inputs will calculate the information
        transmitted from data category 8 variable 12 at time bin 3 to data 
        category 2 variable 4 at time bin 6 about data category 9 variable 
        1 at time bin 3.
    
        

    Variable Inputs:
        (..., 'MCOpt', MCOpt) - specifies whether to use the Monte Carlo method
        to measure a p-value (MCOpt = 'on') or not (MCOpt = 'off') (string)
        (default: 'off')
        (..., 'MCpThresh', MCpThresh) - sets the p-value cutoff. If the
        algorithm finds that the p-value will be above the threshold, the
        calculation ceases (scalar double) (default: 0.001)
        (..., 'MCnSamples', MCnSamples) - sets the number of Monte Carlo trials
        to run (scalar double) (default: 5000)
        (..., 'nullModel', nullModel) - allows the user to use a previously
        established null model probability mass function to calculate
        significance instead of Monte Carlo. This can be especially efficient
        when many information theory analyses have the same underlying null
        model. Note that this option is only currently available for 'PairMI'
        and '2PID'. (structure with fields:
        nullModel.Method - the information theory analysis method (see
            above) used to generate the null distribution (string). This must
            match Method, otherwise the function will employ Monte Carlo, if
            requested.
        nullModel.rFact - a rounding factor used to correct for rounding
            errors near the system resolution (double). The information value
            from the real data will be rounded to the scale set by rFact for
            comparison to the null model pmf.
        nullModel.res - the resolution of the null model (double). This
            value sets the lowest possible p-value for the null model.
        nullModel.nCounts - the number of observations for each state of
            each variable listed in VariableIDs (in order) (cell array with
            each cell as a column vector with state count numbers). This
            information will be compared against the data input into instinfo
            to ensure that the null models are identical. If the null model
            cannot be used, Monte Carlo will be employed, if requested.
        nullModel.info - the unique information values produced by the null
            model (cell array with each cell as a vector of ascending
            information values order matched to the information value outputs
            of instinfo). These values serve as the independent variable in
            the probability mass function. If only one information value is
            supplied, nullModel.info can be a vector instead of a cell.
        nullModel.pmf - the probability of each unique information value
            from the corresponding location in nullModel.info. These values
            serve as the dependent variable in the probability mass function.
            The p-value will be 1 - the cumulative mass function at the
            information value observed in the real data. The type (cell vs.
            vector) and size of nullModel.pmf must match nullModel.info.

    Outputs:
        InfoVal (scalar or vector double) - information value in bits (see
        option set examples above for precise meaning). If multiple
        information values are output (e.g., Method = '2PID'), then InfoVal
        will be a column vector.
        p (scalar or vector double) - p-value from the Monte Carlo estimation
        (will be NaN if the Monte Carlo option is not selected). If multiple
        information values are output (e.g., Method = '2PID'), then p will be
        a column vector corresponding to InfoVal. Note that the minimum
        p-value is set by the resolution from the supplied null model or the
        number of Monte Carlo trials (1/MCnSamples). In both cases, p-value
        results of 0 are reset to half the resolution. 
        MCInfoVals (vector or array double) - information values from the Monte
        Carlo trials (will be NaN if the Monte Carlo option is not selected).
        If multiple information values are output (e.g., Method = '2PID'),
        then MCInfoVals will be a number of information values by number of
        Monte Carlo trials matrix.
        s (double) - a value that indicates whether the Monte Carlo approach
        was used to assess significance (s = 0), the supplied null model was
        used (s = 1), or no significance testing was applied (s = NaN).


        Other m-files required: EntropyY, MutualInfo, TE2, PID
        Subfunctions: none
        MAT-files required: 2PIDMats.mat, 3PIDMats.mat, TE3Redux.mat

    ------

    Version 1.0

    Version Information:

    01/11/2020 - The original version of the program was created before
    and modified it. (QiangLi)

    Copyright (c) 2020, University of Valencia
    All rights reserved.

    Authors: QiangLi (qiang.li@uv.es)
    '''
    import numpy as np
    #Parameters
    MCOpt = 'off'
    MCpThresh = 0.001
    MCnSamples = 5000
    p = NaN
    MCInfoVals = np.nan
    NullModelOpt = 'off'
    s = np.nan
    
    iVarArg = 1
    while iVarArg <= len(*args):
        argOkay = True
        switch(iVarArg)={
            'MCOpt',  MCOpt = varargin{iVarArg+1}; iVarArg = iVarArg + 1
        } 
    
    
    return InfoVal, p, s, MCInfoVals