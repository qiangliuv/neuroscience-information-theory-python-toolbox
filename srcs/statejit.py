def statejit(states, stdev):
    
    '''
    Jitters states with adjacent states in time order, trial order, or
    whatever order they are presented. The jittering is performed using a
    Gaussian distribution with a supplied standard deviation (in units of
    bins). 

    Syntax: [randstates] = statejit(states,std)

    Input:
        states (vector double) - 
        std (scalar) - 

    Outputs:
        randstates (vector double) - randomized version of the input states

    ------

    Version 1.0

    Version Information:

    1.O: 13/08/2020 - The original version of the program was created before
    and modified it. (QiangLi)
  
    Copyright (c) 2020, University of Valencia
    All rights reserved.

    Authors: QiangLi (qiang.li@uv.es)

    -------
    Liences
    -------
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    1. Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.
    
    2. Redistributions in binary form must reproduce the above copyright notice,
        this list of conditions and the following disclaimer in the documentation
        and/or other materials provided with the distribution.

    3. Neither the name of University of Valencia nor the names of its contributors
        may be used to endorse or promote products derived from this software
        without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
    
    '''

    import numpy as np


    waste,I = np.sort((1:len(states)) + stdev*np.randn([1,len(states)]))
    
    randstates = states[I]