def PID(CountsMat,SourcesMat,SetsMat,TransMat):
  '''
  PID calculates the partial information decomposition.

  [PITerms] = PID(CountsMat,SourcesMat,SetsMat,TransMat) is an array that
  contains the partial information terms. It requires several arrays that
  are calculated using PIDLattice. This program is based on the work of 
  Williams and Beer.

  Reference: P. L. Williams and R. D. Beer, arXiv:1004.2515v1 (2010).

  ------
  Inputs:

  CountsMat: An array that contains the counts (or joint probability 
  values) of the various states of the variables. The first index 
  corresponds to the state of the Y variable. The second through N+1 
  indexes correspond to the states of the X1 to XN variables.

  SourcesMat: An array that contains all possible sources for a given set
  of X variables. Each row is a source and each column is a variable.  


  SetsMat: An array that contains the sets of sources in script A. Each 
  row is a set and each column is a source. Sources are numbered 
  according to SourcesMat. i.e. column 1 is source 1 (row 1 on 
  SourcesMat), column 2 is source 2 (row 2 on SourcesMat), etc.

  TransMat: An array that transforms the minimum informations directly 
  to the minimum information terms. The dimensions are r by r (where r 
  is the number of sets of sources). The partial information terms will 
  be found via simple matrix multiplication TransMat*MinimumInfos.

  Outputs:

  PITerms: An array that contains the partial information decomposition
  term values in bits.
  ------

  Version 1.0

  Version Information:

    1.O: 29/06/2020 - The original version of the program was created before
    and modified it. (QiangLi)

  Copyright (c) 2020, University of Valencia
  All rights reserved.

  Authors: Qiang Li (qiang.li@uv.es)

  ----
  Liences
  ----
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 
  2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  3. Neither the name of University of Valencia nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
  '''
  import numpy as np

  #Find the number of states of Y and the X's
  SizeCountsMat = np.size(CountsMat)
  #Find the number of sources and sets of sources
  [NumSets,NumSources] = np.shape(SetsMat)

  #Create a specific information matrix
  SpecificInfos = np.zeros([SizeCountsMat[0],NumSources])

  #Reshape the counts matrix
  NewCountsMat = np.reshape(CountsMat,SizeCountsMat[0], np.prod(SizeCountsMat)/SizeCountsMat[0])
  
  #Find the probabilities for each state of Y
  TotalCounts = np.sum(np.sum(NewCountsMat))
  YCounts = np.sum(NewCountsMat,axis=1)
  Py=YCounts/TotalCounts

  #Now we must go through each source and calculate the appropriate
  #probabilities and the specific information
  for i in range(1, NumSources):
    TempCountsMat = NewCountsMat

    #These are the R variables that aren't in the source
    ToElim=np.argwhere(SourcesMat[i,:] == 0)
    
    #Sum over the variables that aren't in the source
    for j = np.range(1:len(ToElim)):
      TempCountsMat = np.sum(TempCountsMat,ToElim[j]+1)
    if np.size(TempCountsMat,axis=0) != 1:
       TempCountsMat = np.squeeze(TempCountsMat)
    else:
      TempCountsMat = np.squeeze(TempCountsMat)
      TempCountsMat = np.reshape(TempCountsMat, [1, np.size(TempCountsMat)])
    
    #Now reshape the counts matrix to be Y states by A (source) states (A
    #states are combinations of X variables)
    TempSizeCountsMat = np.size(TempCountsMat)
    TempCountsMat = np.reshape(TempCountsMat,TempSizeCountsMat[0], np.prod(TempSizeCountsMat)/TempSizeCountsMat[0])
    
    #Now we must calculate the conditional probabilities
    TempSizeCountsMat = np.size(TempCountsMat)

    #First, a conditional on y
    Pay = np.zeros(TempSizeCountsMat)
    for j in range(1, TempSizeCountsMat[0]):
      Pay[j,:] = TempCountsMat[j,:]/YCounts[j]
    
    #Second, y conditional on a
    Pya = np.zeros(TempSizeCountsMat)
    aCounts = np.sum(TempCountsMat,axis=0)
    for j in range(1, TempSizeCountsMat[1]):
      Pya[:,j] = TempCountsMat[:,j]/aCounts[j]
    
    #Reset infinite values to zero
    Pay(not np.isfinite(Pay)) = 0
    Pya(not np.isfinite(Pya)) = 0

    # Calculate the specific informations
    for j in range(1, TempSizeCountsMat[0]):
      temp1 = np.log2(Pya[j,:]/Py[j])
      temp1(not np.isfinite(temp1)) = 0 
      SpecificInfos[j,i] = Pay[j,:]*np.transpose(temp1)

  #Create a matrix of minimum information values
  MinInfos=np.zeros([NumSets,1])

  #Iterate through each set of sources
  for i in range(1, NumSets):
    #select the relevant specific informations for each set of sources
    RelSpecInfos = SpecificInfos[:,SetsMat[i,:] == 1] 
    #dot the minimum infos with the matching probability
    MinInfos[i] = np.dot(np.min(RelSpecInfos, axis=1), Py)
    
    #Now calculate the partial information terms
    PITerms=TransMat*MinInfos

  return PITerms