# Neuroscience Information Theory Python Toolbox #

## QiangLI

A Python toolbox for performing information theory analyses of neuroscience data developed inspired by original Matlab version. This toolbox contains software to perform numerous information theory analyses. The software is designed primarily with neuroscience applications in mind, but it could easily be applied to data from other research areas.
The first version draft started from: 13 Aug, 2020 -today.

The alpha version toolbox  contains software to perform numerous information theory analysis. The software designed primarily with neuroscience application in mind, 
but it could be applied to data from other research areas. Importantly, neuroscience information theory python toolbox complete based on [Matlab verison](https://github.com/nmtimme/Neuroscience-Information-Theory-Toolbox) which
writed done by [Nicholas Timme](http://www.nicholastimme.com/home.html)


We divided this README file into several important sections:

- Getting started
- Quick started/Use guide
- Fully customizable analysis guide
- Demos
- Detailed functional descriptions
- Simulations

The detail of each section please check [help documents](https://drive.google.com/file/d/1hxb-cuL9CAmv64SRA7ISV8VBoWSwp-wj/view?usp=sharing).

The mathmatics definition of information theory can be check [here](https://bitbucket.org/szzoli/ite/downloads/ITE-0.63_documentation.pdf). This initial **Neuroscience Information Theory Python Toolbox** will be extended to more functions in the future.




### Getting Started 

Download the code to its own folder anywhere on your computer. Then, add the folder that contains the toolbox software to the python search path(more comfortable for me). This can be done by using 'sys.path.insert' in the environment panel of newer versions of Python. 

### Further Help 

For more information about the toolbox, the data formatting, and the demos, please see original Matlab verion ToolboxReadMe.pdf. 

#### Enjoy
The project still ongoing, and welcome to join me to contribute it. Questions, comments, suggestions, or bugs reports should be directed to QiangLi (qiang.li@uv.es).
 